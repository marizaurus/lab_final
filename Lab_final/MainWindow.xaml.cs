﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Lab_final
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        #region opening files

        private void MenuItem_Open(object sender, RoutedEventArgs e)
        {
            string text = Manager.OpenFile();
            if (text == "english")
                MessageBox.Show("Не содержит пригодный для обработки текст!");
            else if (text == "empty")
                MessageBox.Show("Выбранный файл оказался пустым!");
            else if (text == "error")
                MessageBox.Show("Ошибка при открытии файла!");
            else
            {
                origTextBox.Text = text;
                OrigTextBox_CheckColor();
            }
        }

        #endregion

        #region saving files

        private void MenuItem_Save(object sender, RoutedEventArgs e)
        {
            if (resTextBox.Text != "Здесь будет выведен полученный текст...")
                MessageBox.Show(Manager.SaveFile(resTextBox.Text));
            else
                MessageBox.Show("Нечего записывать в файл!");
        }

        #endregion

        #region other menu items

        private void MenuItem_Text_Encode(object sender, RoutedEventArgs e)
        {
            keyWordBox.Text = keyWordBox.Text.ToLower();
            string secretWord = keyWordBox.Text;
            origTextBox.Text = origTextBox.Text.ToLower();
            string line = origTextBox.Text;
            string checkResult = Manager.HasSomeProblems(line, secretWord);
            if (checkResult != "")
            {
                if (checkResult.EndsWith("ключ!"))
                    keyWordBox.Foreground = Brushes.Red;
                else
                    origTextBox.Foreground = Brushes.Red;
                MessageBox.Show(checkResult);
            }
            else
            {
                resTextBox.Text = Manager.Encode(secretWord, line);
                ResTextBox_CheckColor();
            }
        }

        private void MenuItem_Text_Decode(object sender, RoutedEventArgs e)
        {
            keyWordBox.Text = keyWordBox.Text.ToLower();
            string secretWord = keyWordBox.Text;
            origTextBox.Text = origTextBox.Text.ToLower();
            string codedLine = origTextBox.Text;
            string checkResult = Manager.HasSomeProblems(codedLine, secretWord);
            if (checkResult != "")
            {
                if (checkResult == "Введен некорректный ключ!")
                    keyWordBox.Foreground = Brushes.Red;
                else if (checkResult == "Не содержит пригодный для обработки текст!")
                    origTextBox.Foreground = Brushes.Red;
                MessageBox.Show(checkResult);
            }
            else
            {
                resTextBox.Text = Manager.Decode(secretWord, codedLine);
                ResTextBox_CheckColor();
            }
        }

        private void MenuItem_Help(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Краткая инструкция:\n\n" +
                "Введите ключ для шифрования (значение ключа без ввода - \"ключ\");\n" +
                "Введите текст в поле \"Исходный текст\" или загрузите его из файла;\n" +
                "Выберите один из вариантов - шифрование или расшифровка;\n" +
                "Обработанный текст появится в окне \"Полученный текст\";\n" +
                "Сохраните текст в формате txt или docx, указав название и директорию");
        }

        #endregion

        #region useful stuff

        private void Swap_Text(object sender, RoutedEventArgs e)
        {
            string tempText = origTextBox.Text;
            origTextBox.Text = (resTextBox.Text == "Здесь будет выведен полученный текст...")? "" : resTextBox.Text;
            resTextBox.Text = (tempText == "Введите ваш текст...")? "" : tempText;
            ResTextBox_CheckColor();
            OrigTextBox_CheckColor();
        }

        #endregion

        #region setting textBoxes color

        private void KeyWordBox_Loaded(object sender, EventArgs e)
        {
            keyWordBox.Text = "ключ";
            keyWordBox.Foreground = Brushes.Gray;
        }

        private void KeyWordBox_Enter(object sender, EventArgs e)
        {
            if (keyWordBox.Text == "ключ")
                keyWordBox.Text = "";
            keyWordBox.Foreground = Brushes.Black;
        }
        private void KeyWordBox_Leave(object sender, EventArgs e)
        {
            if (keyWordBox.Text == "")
            {
                keyWordBox.Text = "ключ";
                keyWordBox.Foreground = Brushes.Gray;
            }
        }
        
        private void Clear_OrigText(object sender, EventArgs e)
        {
            origTextBox.Text = "Введите ваш текст...";
            origTextBox.Foreground = Brushes.Gray;
        }
        private void OrigTextBox_Enter(object sender, EventArgs e)
        {
            if (origTextBox.Text == "Введите ваш текст...")
                origTextBox.Text = "";
            origTextBox.Foreground = Brushes.Black;
        }
        private void OrigTextBox_Leave(object sender, EventArgs e)
        {
            OrigTextBox_CheckColor();
        }
        private void OrigTextBox_CheckColor()
        {
            if (origTextBox.Text == "" || origTextBox.Text == "Введите ваш текст...")
            {
                origTextBox.Text = "Введите ваш текст...";
                origTextBox.Foreground = Brushes.Gray;
            }
            else
                origTextBox.Foreground = Brushes.Black;
        }
        
        private void Clear_ResText(object sender, EventArgs e)
        {
            resTextBox.Text = "Здесь будет выведен полученный текст...";
            resTextBox.Foreground = Brushes.Gray;
        }
        private void ResTextBox_CheckColor()
        {
            if (resTextBox.Text == "" || resTextBox.Text == "Здесь будет выведен полученный текст...")
            {
                resTextBox.Text = "Здесь будет выведен полученный текст...";
                resTextBox.Foreground = Brushes.Gray;
            }
            else
                resTextBox.Foreground = Brushes.Black;
        }

        #endregion
    }
}