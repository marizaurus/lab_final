﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Win32;
using System.Reflection;

namespace Lab_final
{
    public class Manager
    {
        static string russianAlph = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        static int l = russianAlph.Length;
        
        static Object missObj = Missing.Value;
        static Object trueObj = true;
        static Object falseObj = false;

        #region opening

        public static string OpenFile()
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                DefaultExt = ".txt",
                Filter = "Text files (.txt)|*.txt|Word Documents(*.doc, *docx)|*.doc;*docx",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };
            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string filename = dialog.FileName;
                if (filename.EndsWith(".txt"))
                    return OpenExistingTxt(filename);
                else if (filename.EndsWith(".docx") || filename.EndsWith(".doc"))
                    return OpenExistingWord(filename);
                else
                    return "error";
            }
            else
                return "error";
        }

        public static string OpenExistingTxt(string filename)
        {
            if (new FileInfo(filename).Length != 0)
            {
                string text = File.ReadAllText(filename, Encoding.UTF8).ToLower();
                if (text.Trim() == "")
                    return "empty";
                if (NotContainsRussian(text))
                    return "english";
                else
                    return text;
            }
            else
                return "empty";
        }

        public static string OpenExistingWord(string filename)
        {
            if (new FileInfo(filename).Length != 0)
            {
                Word.Application wordApp = new Word.Application();
                Word.Document document;
                Object pathObj = filename;
                try
                {
                    document = wordApp.Documents.Add(ref pathObj, ref missObj, ref missObj, ref missObj);
                }
                catch (Exception error)
                {
                    wordApp.Quit(ref missObj, ref missObj, ref missObj);
                    document = null;
                    wordApp = null;
                    throw error;
                }
                Word.Range wRange = document.Range(ref missObj, ref missObj);
                string text = wRange.Text.ToLower();
                document.Close(ref falseObj, ref missObj, ref missObj);
                wordApp.Quit();

                if (text.Trim() == "")
                    return "empty";
                else if (NotContainsRussian(text))
                    return "english";
                else
                    return text;
            }
            else
                return "empty";
        }

        #endregion

        #region saving

        public static string SaveFile(string resText)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                DefaultExt = ".txt",
                OverwritePrompt = true,
                Filter = "Text files (.txt)|*.txt|Word Documents(*.doc, *docx)|*.doc;*docx",
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            };

            Nullable<bool> result = dialog.ShowDialog();
            if (result == true)
            {
                string filename = dialog.FileName;
                if (filename.EndsWith(".txt"))
                {
                    File.WriteAllText(filename, resText, Encoding.UTF8);
                    return "Файл сохранен в формате txt";
                }
                else
                {
                    CreateWord(resText, filename);
                    return "Файл сохранен в формате doc/docx";
                }
            }
            else
                return "Произошла ошибка при сохранении файла!";
        }

        public static void CreateWord(string text, string wordfilename)
        {
            Word.Application wordApp = new Word.Application();
            Object documentType = Word.WdNewDocumentType.wdNewBlankDocument;
            wordApp.Documents.Add(ref missObj, ref falseObj, ref documentType, ref trueObj);
            Object templateObj = wordfilename;
            Word.Document document = wordApp.Documents.Add(ref missObj, ref missObj, ref missObj, ref missObj);
            object start = 0;
            object end = 0;

            Word.Range rng = document.Range(ref start, ref end);
            rng.Text = text;

            document.SaveAs(ref templateObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj,
                ref missObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj, ref missObj);
            document.Close(ref trueObj, ref missObj, ref missObj);
            wordApp.Quit();
        }

        #endregion

        #region encoding & decoding methods

        public static string Encode(string secretWord, string line)
        {
            int k = 0;
            string codedLine = "";                             //шифрование
            for (int i = 0; i < line.Length; i++)
            {
                if (russianAlph.Contains(line[i]))
                {
                    int keyCharPos = russianAlph.IndexOf(secretWord[k % secretWord.Length]);
                    int decodedCharPos = russianAlph.IndexOf(line[i]);
                    codedLine += russianAlph[(decodedCharPos + keyCharPos) % l];
                    k++;
                }
                else
                    codedLine += line[i];
            }
            return codedLine;
        }

        public static string Decode(string secretWord, string codedLine)
        {
            int k = 0;                                          //расшифровка
            string decodedLine = "";
            for (int i = 0; i < codedLine.Length; i++)
            {
                if (russianAlph.Contains(codedLine[i]))
                {
                    int keyCharPos = russianAlph.IndexOf(secretWord[k % secretWord.Length]);
                    int codedCharPos = russianAlph.IndexOf(codedLine[i]);
                    decodedLine += russianAlph[(codedCharPos - keyCharPos + l) % l];
                    k++;
                }
                else
                    decodedLine += codedLine[i];
            }
            return decodedLine;
        }

        #endregion

        #region useful

        public static bool NotContainsRussian(string text)
        {
            int count = 0;
            foreach (char c in russianAlph)
            {
                if (!text.Contains(c))
                {
                    count++;
                    continue;
                }
            }
            return count == l;
        }

        public static string HasSomeProblems(string text, string key)
        {
            if (NotContainsRussian(key) || (key.Length == 0))
                return "Введен некорректный ключ!";
            else if (NotContainsRussian(text) || (text.Length == 0))
                return "Не содержит пригодный для обработки текст!";
            else
                return "";
        }

        #endregion
    }
}
