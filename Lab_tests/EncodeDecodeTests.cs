﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab_final;

namespace LabTests
{
    [TestClass]
    public class EncodeDecodeTests
    {
        [TestMethod]
        public void Encode_NotRussian()
        {
            string key = "ключ";            //key was already checked at MainWindow.MenuItem_Text_Encode
            string text = "kdjfgndnfjg";
            string expText = "kdjfgndnfjg";
            Assert.AreEqual(expText, Manager.Encode(key, text));
        }

        [TestMethod]
        public void Decode_NotRussian()
        {
            string key = "ключ";
            string text = "kdjfgndnfjg";
            string expText = "kdjfgndnfjg";
            Assert.AreEqual(expText, Manager.Decode(key, text));
        }

        [TestMethod]
        public void Encode_Normal()
        {
            string key = "яблоко";
            string text = "от яблони недалеко падает";
            string expText = "ну кпцэмй щуоокёцэ ъогбрб";
            Assert.AreEqual(expText, Manager.Encode(key, text));
        }

        [TestMethod]
        public void Decode_Normal()
        {
            string key = "яблоко";
            string text = "ну кпцэмй щуоокёцэ ъогбрб";
            string expText = "от яблони недалеко падает";
            Assert.AreEqual(expText, Manager.Decode(key, text));
        }

        [TestMethod]
        public void Encode_Mixed()
        {
            string key = "яблоко";
            string text = "от яблони abc недалеко 123 падает";
            string expText = "ну кпцэмй abc щуоокёцэ 123 ъогбрб";
            Assert.AreEqual(expText, Manager.Encode(key, text));
        }

        [TestMethod]
        public void Decode_Mixed()
        {
            string key = "яблоко";
            string text = "ну кпцэмй abc щуоокёцэ 123 ъогбрб";
            string expText = "от яблони abc недалеко 123 падает";
            Assert.AreEqual(expText, Manager.Decode(key, text));
        }
    }
}
