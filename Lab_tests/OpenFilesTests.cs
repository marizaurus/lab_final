﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Lab_final;
using System;
using Word = Microsoft.Office.Interop.Word;
using System.Text;
using System.Reflection;

namespace LabTests
{
    [TestClass]
    public class OpenFilesTests
    {
        public static string filename = "testTxt.txt";
        private static string executableLocation = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        private static string wordfilename = Path.Combine(executableLocation, "testWord.docx");
        private static Object missObj = Missing.Value;
        private static Object trueObj = true;
        private static Object falseObj = false;

        [TestMethod]
        public void EnglishTxt_ToProcess()
        {
            File.WriteAllText(filename, "hello world", Encoding.UTF8);
            Assert.AreEqual("english", Manager.OpenExistingTxt(filename));
            File.Delete(filename);
        }

        [TestMethod]
        public void EmptyTxt_ToProcess()
        {
            File.WriteAllText(filename, "", Encoding.UTF8);
            Assert.AreEqual("empty", Manager.OpenExistingTxt(filename).Trim());
            File.Delete(filename);
        }

        [TestMethod]
        public void NormalTxt_ToProcess()
        {
            File.WriteAllText(filename, "правильный текст", Encoding.UTF8);
            Assert.AreEqual("правильный текст", Manager.OpenExistingTxt(filename));
            File.Delete(filename);
        }

        [TestMethod]
        public void BigTxt_ToProcess()
        {
            File.WriteAllText(filename, "БОлЬШоЙ ТЕкСТ", Encoding.UTF8);
            Assert.AreEqual("большой текст", Manager.OpenExistingTxt(filename));
            File.Delete(filename);
        }


        [TestMethod]
        public void EnglishWord_ToProcess()
        {
            Manager.CreateWord("hello world", wordfilename);
            Assert.AreEqual("english", Manager.OpenExistingWord(wordfilename));
            File.Delete(wordfilename);
        }

        [TestMethod]
        public void EmptyWord_ToProcess()
        {
            Manager.CreateWord("", wordfilename);
            Assert.AreEqual("empty", Manager.OpenExistingWord(wordfilename));
            File.Delete(wordfilename);
        }

        [TestMethod]
        public void NormalWord_ToProcess()
        {
            Manager.CreateWord("правильный текст", wordfilename);
            Assert.AreEqual("правильный текст", Manager.OpenExistingWord(wordfilename).Trim());
            File.Delete(wordfilename);
        }
    }
}
